ans = primes !! 10000

primes = sieve [2..] where sieve (p:xs) = p : sieve (filter (\x -> mod x p /= 0) xs
	
main = putStrLn (show ans)
