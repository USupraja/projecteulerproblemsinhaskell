def letter_count(lst, ele):
    count = 0
    for i in lst:
        if (i == ele):
            count = count + 1
            if (count == 2):
                return False
    return True

def ele_check(ele_lst):
    x = True
    for i in ele_lst:
        if ((x and letter_count(ele_lst, i)) == False):
            return False
    return True

def order(a):
    c = list(a)
    b = list(a)
    b.sort()
    return ((c == b) and (ele_check(c)))

def odometer(start, limit):
    lst = []
    for i in range(start, limit):
        if (order(str(i))):
            lst.append(i)
    return lst

def diff(x, y):
    lst = odometer(x, y + 1)
    return (lst.index(y) - lst.index(x))
 
print(odometer(1, 9))
print(diff(1, 9))